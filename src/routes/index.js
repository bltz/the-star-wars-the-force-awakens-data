import React, { Component, Suspense } from "react";
import { connect } from "react-redux";
import { PrivateRoute, PublicRoute } from './routes';
import { menus } from './menus';
import Spin from 'antd/lib/spin';
import Verify from '../containers/Verify'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { checkVerifyData } from '../redux/actions/userAction'


class Routes extends Component {
  constructor(){
    super()
    this.state = {
      _verifyCheck : false
    }
  }
  UNSAFE_componentWillMount(){
    return this.props.checkVerifyDataAction(
      (() => this.setState({ _verifyCheck : true })),
      (() => this.setState({ _verifyCheck : true })),
    )
  }
  UNSAFE_componentWillReceiveProps(nextProps){
    document.body.style.backgroundColor = nextProps.config.theme.backgroundColor;
    document.body.style.color           = nextProps.config.theme.color;
  }
  render(){
    const { user, config } = this.props
    return(
      <div>
        <Suspense fallback={<div className="centered"><Spin size="large"/></div>}>
        {
          this.state._verifyCheck && 
          <Router>
            <Switch>
              {/* <Route path='/verify' exact={true} component={Verify} /> */}
              {
                menus.map((menu,index) => {
                  return menu.private 
                         ? <PrivateRoute key={index} verify={user} parent={menu.parent ? menu.parent : null } content={menu.content ? menu.content : null}  path={menu.path} component={menu.lazyComponent} exact/>
                         : <PublicRoute key={index} verify={user} parent={menu.parent ? menu.parent : null } content={menu.content ? menu.content : null}  path={menu.path} component={menu.lazyComponent} exact/>
                  // return <PrivateRoute key={index} verify={user} parent={menu.parent ? menu.parent : null } content={menu.content ? menu.content : null}  path={menu.path} component={menu.lazyComponent} exact/>
                })
              }
              <Route path='*' exact={true} render={() => (<h2>not found</h2>)} />
            </Switch>
        </Router>
        }
        </Suspense>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    config : state.config,
    user : state.user
  }
}

const mapActionsToProps = {
  checkVerifyDataAction : checkVerifyData
}

export default connect(mapStateToProps,mapActionsToProps)(Routes);