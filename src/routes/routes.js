import React from 'react';
import { Route, Redirect } from "react-router-dom";
import Main from '../layouts/Main';

const PrivateRoute = ({ component : Component, ...rest}) => {
  return <Route {...rest}
            render={(props) => {
              return rest.verify.valid && rest.verify.uuid !== null && rest.verify.name !== null
                     ? <Main>
                        <Component {...props} content={rest.content} parent={rest.parent}/>
                      </Main>
                     : <Redirect to='/verify'/>
            }}
         />
}

const PublicRoute =  ({ component: Component, verified, authed, ...rest}) => {
  return (
    <Route {...rest}
      render={(props) => 
        rest.verify.valid && rest.verify.uuid !== null && rest.verify.name !== null 
        ? <Redirect to={`/`} /> : <Component {...props} />
      }
    />
  )
}

export {
  PrivateRoute,
  PublicRoute
}