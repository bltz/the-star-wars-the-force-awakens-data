import React from 'react';

export const menus = [ 
  {
    type : 'parent',
    private : false,
    path : '/verify',
    initial : 'Verify',
    lazyComponent : React.lazy(() => import('../containers/Verify'))
  },
  {
    type : 'parent',
    private : true,
    path : '/',
    initial : 'Home',
    lazyComponent : React.lazy(() => import('../containers/Home'))
  },
  {
    type : 'parent',
    private : true,
    path : '/people',
    initial : 'people',
    lazyComponent : React.lazy(() => import('../containers/ParentContainer')),
    content : {
      main : { type : 'plural', key : 'people', pushData : true },
      leftBar : { type : 'plural', key : 'species', page : 1, lengthIndex : 6 },
      rightBar : { type : 'plural', key : 'films', page : 1, lengthIndex : 5 },
      adv : {
        source : 'https://tpc.googlesyndication.com/simgad/9370114722623079948',
        altText : 'pure taste',
        href : 'https://www.simplyauthentic.id/rewards/se/wxwKQMOtmjWd9G2qsbNlDQb9E52PgrVQwUId9UlUz2ZcIQDANCj2HSs66aq29SjREGdc1Uf8PcfvvwlcGtw?utm_source=PDI_Publisher&utm_medium=PDI_Kumparan&utm_campaign=PDI_Kumparan_CLMSE',
        rel : 'nofollow',
        footer : true
      } 
    }
  },
  {
    type : 'child',
    parent : 'people',
    private : true,
    path : '/people/:slug',
    initial : 'person',
    lazyComponent : React.lazy(() => import('../containers/ChildContainer')),
    content : {
      rightBar : { type : 'plural', key : 'planets', page : 1, pushData : false, lengthIndex : 3 },
    }
  },
  {
    type : 'parent',
    private : true,
    path : '/films',
    initial : 'Films',
    lazyComponent : React.lazy(() => import('../containers/ParentContainer')),
    content : {
      main : { type : 'plural', key : 'films', pushData : true },
      leftBar : { type : 'plural', key : 'starships', page : 1, lengthIndex : 6 },
      rightBar : { type : 'plural', key : 'planets', page : 1, lengthIndex : 5 },
      adv : {
        source : 'https://tpc.googlesyndication.com/simgad/14921153086685689129',
        altText : 'i dare',
        href : 'https://www.youtube.com/watch?v=wd2nyEG0-vw',
        rel : 'nofollow',
        footer : true
      }  
    }
  },
  {
    type : 'child',
    parent : 'films',
    private : true,
    path : '/films/:slug',
    initial : 'Film',
    lazyComponent : React.lazy(() => import('../containers/ChildContainer')),
    content : {
      rightBar : { type : 'plural', key : 'people', page : 1, pushData : false, lengthIndex : 3 },
    }
  },
  {
    type : 'parent',
    private : true,
    path : '/starships',
    initial : 'Starships',
    lazyComponent : React.lazy(() => import('../containers/ParentContainer')),
    content : {
      main : { type : 'plural', key : 'starships', pushData : true },
      leftBar : { type : 'plural', key : 'vehicles', page : 1, lengthIndex : 6 },
      rightBar : { type : 'plural', key : 'films', page : 1, lengthIndex : 5 },
      adv : {
        source : 'https://tpc.googlesyndication.com/simgad/9370114722623079948',
        altText : 'pure taste',
        href : 'https://www.simplyauthentic.id/rewards/se/wxwKQMOtmjWd9G2qsbNlDQb9E52PgrVQwUId9UlUz2ZcIQDANCj2HSs66aq29SjREGdc1Uf8PcfvvwlcGtw?utm_source=PDI_Publisher&utm_medium=PDI_Kumparan&utm_campaign=PDI_Kumparan_CLMSE',
        rel : 'nofollow',
        footer : true
      }  
    }
  },
  {
    type : 'child',
    parent : 'starships',
    private : true,
    path : '/starships/:slug',
    initial : 'Starship',
    lazyComponent : React.lazy(() => import('../containers/ChildContainer')),
    content : {
      rightBar : { type : 'plural', key : 'vehicles', page : 1, pushData : false, lengthIndex : 3 },
    }
  },
  {
    type : 'parent',
    private : true,
    path : '/vehicles',
    initial : 'Vehicles',
    lazyComponent : React.lazy(() => import('../containers/ParentContainer')),
    content : {
      main : { type : 'plural', key : 'vehicles', pushData : true },
      leftBar : { type : 'plural', key : 'starships', page : 1, lengthIndex : 6 },
      rightBar : { type : 'plural', key : 'planets', page : 1, lengthIndex : 5 },
      adv : {
        source : 'https://tpc.googlesyndication.com/simgad/14921153086685689129',
        altText : 'i dare',
        href : 'https://www.youtube.com/watch?v=wd2nyEG0-vw',
        rel : 'nofollow',
        footer : true
      } 
    }
  },
  {
    type : 'child',
    parent : 'vehicles',
    private : true,
    path : '/vehicles/:slug',
    initial : 'Vehicle',
    lazyComponent : React.lazy(() => import('../containers/ChildContainer')),
    content : {
      rightBar : { type : 'plural', key : 'starships', page : 1, pushData : false, lengthIndex : 3 },
    }
  },
  {
    type : 'parent',
    private : true,
    path : '/species',
    initial : 'Species',
    lazyComponent : React.lazy(() => import('../containers/ParentContainer')),
    content : {
      main : { type : 'plural', key : 'species', pushData : true },
      leftBar : { type : 'plural', key : 'people', page : 1, lengthIndex : 6 },
      rightBar : { type : 'plural', key : 'films', page : 1, lengthIndex : 5 },
      adv : {
        source : 'https://tpc.googlesyndication.com/simgad/9370114722623079948',
        altText : 'pure taste',
        href : 'https://www.simplyauthentic.id/rewards/se/wxwKQMOtmjWd9G2qsbNlDQb9E52PgrVQwUId9UlUz2ZcIQDANCj2HSs66aq29SjREGdc1Uf8PcfvvwlcGtw?utm_source=PDI_Publisher&utm_medium=PDI_Kumparan&utm_campaign=PDI_Kumparan_CLMSE',
        rel : 'nofollow',
        footer : true
      }  
    }
  },
  {
    type : 'child',
    parent : 'species',
    private : true,
    path : '/species/:slug',
    initial : 'Species',
    lazyComponent : React.lazy(() => import('../containers/ChildContainer')),
    content : {
      rightBar : { type : 'plural', key : 'people', page : 1, pushData : false, lengthIndex : 3 },
    }
  },
  {
    type : 'parent',
    private : true,
    path : '/planets',
    initial : 'Planets',
    lazyComponent : React.lazy(() => import('../containers/ParentContainer')),
    content : {
      main : { type : 'plural', key : 'planets', pushData : true },
      leftBar : { type : 'plural', key : 'species', page : 1, lengthIndex : 6 },
      rightBar : { type : 'plural', key : 'films', page : 1, lengthIndex : 5 },
      adv : {
        source : 'https://tpc.googlesyndication.com/simgad/14921153086685689129',
        altText : 'i dare',
        href : 'https://www.youtube.com/watch?v=wd2nyEG0-vw',
        rel : 'nofollow',
        footer : true
      }  
    }
  },
  {
    type : 'child',
    parent : 'planets',
    private : true,
    path : '/planets/:slug',
    initial : 'Planet',
    lazyComponent : React.lazy(() => import('../containers/ChildContainer')),
    content : {
      rightBar : { type : 'plural', key : 'starships', page : 1, pushData : false, lengthIndex : 3 },
    }
  }
]