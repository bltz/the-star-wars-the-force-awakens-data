import React from 'react';
import Navigation from '../components/Navigation';
import { CustomizeTheme } from '../components/CustomizeTheme';
import Footer from '../components/Footer';
import { responsive } from '../helper'
class Main extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      width: 0
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions.bind(this));
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }
  
  render(){
    const { children } = this.props;
    const { height, width } = this.state;
    return(
      <React.Fragment>
        <div>
          <Navigation height={height} width={width}/>
          <div>
            { 
              React.Children.map( children, child =>
                React.cloneElement(child, { currentSize: { height, width } })
              ) 
            }
          </div>
        </div>
        <CustomizeTheme/>
        { width <= responsive.mobileSizeLimit && <div style={{ marginLeft : '30px', marginRight : '30px', marginBottom : '30px' }}><Footer/></div> }
      </React.Fragment>
    )
  }
  
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions.bind(this));
  }
}

export default Main