import React, { Component } from "react";
import { connect } from "react-redux";
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import RightBar from '../components/sidebar/RightBar';
import { responsive } from '../helper';

class MainChildContainer extends Component{
  render(){
    const {  width, height, rightBar, children, history } = this.props;
    return(
      <Row style={{ marginTop : 20 }}>
        <Col span={width <= responsive.mobileSizeLimit ? 22 : 16} offset={width <= responsive.mobileSizeLimit ? 1 : 4}>
          <Row gutter={[16, 16]}>
            <Col span={width <= responsive.mobileSizeLimit ? 24 : 17}>
            { children }
            </Col>
            {
              rightBar.active &&
              <RightBar
                history={history}
                col={{
                  span : width <= responsive.mobileSizeLimit ? 24 : 7
                }}
                topBox={{
                  requestListLength : rightBar.requestListLength,
                  type : rightBar.type,
                  initialState : this.props[rightBar.contentIndicator],
                  indicator : rightBar.contentIndicator,
                  affix : {
                    header :  width <= responsive.mobileSizeLimit ? true : false,
                    wraper : false
                  },
                  title : rightBar.contentIndicator
                }}
                advBox={{
                  affix : width <= responsive.mobileSizeLimit ? false : true,
                  responsiveAccurate:{ width, height },
                  options:{
                    source : rightBar.advOptions.source,
                    altText : rightBar.advOptions.altText,
                    href : rightBar.advOptions.href,
                    rel : rightBar.advOptions.rel
                  },
                  footer:{ show : rightBar.advOptions.footer.show }
                }}
              />
            }
          </Row>
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    config    : state.config,
    people    : state.people,
    films     : state.films,
    planets   : state.planets,
    starships : state.starships,
    vehicles  : state.vehicles,
    species   : state.species
  }
}

export default connect(mapStateToProps)(MainChildContainer)