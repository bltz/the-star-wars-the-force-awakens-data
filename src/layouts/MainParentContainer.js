import React, { Component } from "react";
import { connect } from "react-redux";
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import RightBar from '../components/sidebar/RightBar';
import LeftBar from '../components/sidebar/LeftBar'
import { responsive } from '../helper';

class MainParentContainer extends Component {
  constructor(props){
    super(props)
  }
  getDefaultNumberOfListSkeleton(key){
    return key === 'films' ? 7 : key === 'planets' ? 5 : 10
  }
  render() {
    const {  type, width, height, leftBar, rightBar, children } = this.props;
    return(
      <Row style={{ marginTop : 20 }}>
        <Col span={width <= responsive.mobileSizeLimit ? 22 : 16} offset={width <= responsive.mobileSizeLimit ? 1 : 4}>
          <Row gutter={[16, 16]}>
            {
              leftBar.active &&
              <LeftBar
                history={this.props.history}
                col={{
                  span : width <= responsive.mobileSizeLimit ? 24 : 7
                }}
                topBox={{
                  requestListLength : leftBar.requestListLength,
                  type : leftBar.type,
                  initialState : this.props[leftBar.contentIndicator],
                  indicator : leftBar.contentIndicator,
                  affix : {
                    header :  width <= responsive.mobileSizeLimit ? true : false,
                    wraper : width <= responsive.mobileSizeLimit ? false : true
                  },
                  title : leftBar.contentIndicator
                }}
              />
            }
            <Col span={width <= responsive.mobileSizeLimit ? 24 : 10}>
              { children }
            </Col>
            {
              rightBar.active &&
              <RightBar
                history={this.props.history}
                col={{
                  span : width <= responsive.mobileSizeLimit ? 24 : 7
                }}
                topBox={{
                  requestListLength : rightBar.requestListLength,
                  type : rightBar.type,
                  initialState : this.props[rightBar.contentIndicator],
                  indicator : rightBar.contentIndicator,
                  affix : {
                    header :  width <= responsive.mobileSizeLimit ? true : false,
                    wraper : false
                  },
                  title : rightBar.contentIndicator
                }}
                advBox={{
                  affix : width <= responsive.mobileSizeLimit ? false : true,
                  responsiveAccurate:{ width, height },
                  options:{
                    source : rightBar.advOptions.source,
                    altText : rightBar.advOptions.altText,
                    href : rightBar.advOptions.href,
                    rel : rightBar.advOptions.rel
                  },
                  footer:{ show : rightBar.advOptions.footer.show }
                }}
              />
            }
          </Row>
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    config    : state.config,
    people    : state.people,
    films     : state.films,
    planets   : state.planets,
    starships : state.starships,
    vehicles  : state.vehicles,
    species   : state.species
  }
}

export default connect(mapStateToProps)(MainParentContainer);