import axios from 'axios';
export const fetchData = (options) => axios.get(options.type === 'plural' ? `${process.env.HTTP_URL}${options.key}?page=${options.page}` : options.url)
export const fetchDataSingularLoop = ( request, callback ) => {
  return request.forEach((req,i) => {
    return fetchData({ type : 'singular', url : req }).then((res) => {
      return callback && callback(res)
    })
  })
}