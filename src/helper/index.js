import { mainColors } from './mainColor';
import { responsive } from './responsive';
import { minMaxNumberRandom } from './minMaxNumberRandom';
import { stringCapital } from './stringCustomize';
import { getQueryString, getResourceParams, makeTitle } from './queryString';

export {
  mainColors,
  responsive,
  minMaxNumberRandom,
  stringCapital,
  getQueryString,
  getResourceParams, 
  makeTitle
}