export const mainColors = {
  bgDark : '#1d1d1d',
  accentBgDark : '#282828',
  accentBgLight : '#fff',
  bgLight : '#fff',
  clDark : '#1d1d1d',
  clLight : '#fff',
  navBgDark : '#000',
  navBgLight : '#fff',
  headIndexDark : '#000',
  headIndexLight : '#f8f8f8',
  borderColorDark : '#2b2b2b',
  borderColorLight : '#dfe1e5'
}