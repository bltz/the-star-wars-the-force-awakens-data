export function stringCapital(str){
  return str.toLowerCase()
  .replace(/(^|[^a-z0-9-])([a-z])/g,
    function(m, m1, m2, p) {
      return m1 + m2.toUpperCase();
    });
}