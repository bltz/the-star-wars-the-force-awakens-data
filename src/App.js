import React from 'react';
import { Provider } from 'react-redux';
import '../public/styles/main.css';
import '../public/styles/main.scss';
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import Routes from './routes';
import BackTop from 'antd/lib/back-top'

class App extends React.Component {
  render() {
  const { store } = this.props
  document.body.style.backgroundColor = store.getState().config.theme.backgroundColor;
  document.body.style.color           = store.getState().config.theme.color;
  return(
    <Provider store={store}>
      <BackTop>
        <div className="ant-back-top-inner">UP</div>
      </BackTop>
      <Routes/>
    </Provider>
    );
  }
}

export default App