import React from 'react';
import { connect } from "react-redux";
import { infiniteLoadFinish, getDataContainerPage, resetCurrentContainerData } from '../redux/actions/fetchDataAction';
import MainParentContainer from '../layouts/MainParentContainer';
import InfiniteScroll from 'react-infinite-scroller';
import CardBoxInfiniteLoad from '../components/CardBoxInfiniteLoad';
import {List} from '../components/list/List';
import { ListSkeleton } from '../components/contentLoader';
import { responsive } from '../helper';
import { spinnerON, spinnerOFF } from '../redux/actions/configAction';
import { getDetailIndex } from '../redux/actions/detailAction';
import { unmountDetailPage } from '../redux/actions/detailAction';
const ListRender = List()

class ParentContainer extends React.Component{
  UNSAFE_componentWillMount(){
    return this.props.unmountDetailPageAction()
  }
  render(){
    const { config, currentSize : { width, height }, people, content, history, spinnerONaction, spinnerOFFaction, getDetailIndexAction } = this.props;
    let items = [];
    this.props[content.main.key]['datas'].map((data, dataIndex) => {
        items.push(
        <div key={dataIndex}>
          {
            ListRender[content.main.key](
              config, data, dataIndex, null,'infinite', history,
              { spinnerONaction, spinnerOFFaction, getDetailIndexAction },
              items.length
            )
          }
        </div>
        );
    });
    return(
      <MainParentContainer
        history={this.props.history}
        width={ width }
        height={ height }
        leftBar={{
          contentIndicator : `${content.leftBar.key}`,
          requestListLength : content.leftBar.lengthIndex,
          type: 'page',
          active : width <= responsive.mobileSizeLimit ? false : true
        }}
        rightBar={{
          contentIndicator : `${content.rightBar.key}`,
          requestListLength : content.rightBar.lengthIndex,
          type: 'page',
          active : width <= responsive.mobileSizeLimit ? true : true,
          advOptions:{
            source : `${content.adv.source}`,
            altText : `${content.adv.altText}`,
            href : `${content.adv.href}`,
            rel : `${content.adv.rel}`,
            footer : {
              show : content.adv.footer
            }
          }
        }}
      >
        <CardBoxInfiniteLoad
          load={this.props[content.main.key].load}
          config={config}
          affix={{
            header : true,
            wraper : false
          }}
          title={`${content.main.key}`}
        >
          <InfiniteScroll
            pageStart={0}
            loadMore={(page) => this.infiniteLoad(page)}
            hasMore={this.props[content.main.key].hasMore}
            threshold={-35}
            useWindow={true}
            loader={ 
              <ListSkeleton 
                key={0}
                body={{ 
                  paragraph : {
                    title : true, 
                    rows : 1 , 
                  },
                  avatar : {
                    show : false,
                    size : 'large',
                    shape : 'circle'
                  },
                  active : true,
                  numberOfList : this.props[content.main.key].currentPage === 0 ? 7 : 2 
                }}
              />
            }
          >
              {items} 
          </InfiniteScroll>
        </CardBoxInfiniteLoad>
      </MainParentContainer>
    )
  }
  infiniteLoad(currentPage){
    const { getDataContainerPageAction, content } = this.props
    let temp = 0
    return setTimeout(() => {
      return getDataContainerPageAction([
          { type : `${content.main.type}`, key : `${content.main.key}`, page : currentPage, pushData : true }
        ], 
        (() =>  temp === 0 && ( getDataContainerPageAction([ content.leftBar, content.rightBar ]), temp++ ))
      ) 
    }, temp === 0 ? 500 : 1000 )
  }
  componentWillUnmount(){
    const { resetCurrentContainerDataAction, content } = this.props
    return resetCurrentContainerDataAction( 
      [ content.leftBar, content.rightBar ],
      (() => resetCurrentContainerDataAction([{type : `${content.main.type}`, key : `${content.main.key}`}]) ) 
    )
  }
}

const mapStateToProps = (state) => {
  return {
    config    : state.config,
    people    : state.people,
    films     : state.films,
    planets   : state.planets,
    starships : state.starships,
    vehicles  : state.vehicles,
    species   : state.species
  }
}

const mapActionsToProps = {
  infiniteLoadFinishAction : infiniteLoadFinish,
  resetCurrentContainerDataAction : resetCurrentContainerData,
  getDataContainerPageAction : getDataContainerPage,
  spinnerONaction : spinnerON,
  spinnerOFFaction : spinnerOFF,
  getDetailIndexAction : getDetailIndex,
  unmountDetailPageAction : unmountDetailPage
}

export default connect(mapStateToProps, mapActionsToProps)(ParentContainer);