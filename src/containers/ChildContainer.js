import React, { Component } from "react";
import { connect } from "react-redux";
import MainChildContainer from '../layouts/MainChildContainer';
import { responsive, getQueryString, getResourceParams, makeTitle } from '../helper';
import { getDataContainerPage, resetCurrentContainerData } from '../redux/actions/fetchDataAction';
import BreadcrumbHistory from '../components/BreadcrumbHistory'
import { withRouter } from 'react-router-dom';
import { unmountDetailPage } from '../redux/actions/detailAction';
import Spin from 'antd/lib/spin';
import { getDetailIndexForceRefresh } from '../redux/actions/detailAction';
import { VehiclesDetailWrapper, FilmsDetailWrapper, PeopleDetailWrapper, PlanetsDetailWrapper, SpeciesDetailWrapper, StarshipsDetailWrapper } from '../components/childWrapper'

class ChildContainer extends Component{
  getDetailContent(){
    return this.props.getDetailIndexForceRefreshAction(
      getQueryString(this.props.location.search),
      getResourceParams(this.props.location.pathname),
      makeTitle(this.props.match.params.slug)
    )
  }
  componentDidMount(){
    const { content } = this.props;
    return this.props.getDataContainerPageAction([content.rightBar])
  }
  render(){
    const { detail, config, currentSize : { width, height }, content, parent, history } = this.props;
    detail.status === 0 && this.getDetailContent()
    return(
    <MainChildContainer
      history={history}
      width={width}
      height={height}
      rightBar={{
        contentIndicator : `${content.rightBar.key}`,
        requestListLength : content.rightBar.lengthIndex,
        type: 'page',
        active : width <= responsive.mobileSizeLimit ? true : true,
        advOptions:{
          source : 'https://tpc.googlesyndication.com/simgad/14921153086685689129',
          altText : 'i dare',
          href : 'https://www.youtube.com/watch?v=wd2nyEG0-vw',
          rel : 'nofollow',
          footer : {
            show : true
          }
        }
      }}
    >
      <div>
        <BreadcrumbHistory
          config={config}
          parent={parent}
          current={detail.breadcrumbTitle}
        />
        <div className="box-border" style={{
          marginTop : 10,
          borderColor : config.theme.borderColor,
          padding : '20px',
          backgroundColor : config.theme.accentBg,
          display : 'flex',
          justifyContent : detail.status === 200 ? 'flex-start' : 'center',
          alignItems : detail.status === 200 ? 'flex-start' : 'center',
          height : detail.status === 200 ? 'auto' : width <= responsive.mobileSizeLimit ? 450 : 650
        }}>
        { detail.status !== 200 ? <Spin tip="Loading..."/> : this.getContentSkeleton(detail.wrapper,history) }
        </div>
      </div> 
    </MainChildContainer> 
    )
  }
  getContentSkeleton(wrapper,history){
    switch(wrapper){
      case 'peopleWrapper':
        return <PeopleDetailWrapper history={history}/>
      case 'filmsWrapper':
        return <FilmsDetailWrapper history={history}/>
      case 'planetsWrapper':
        return <PlanetsDetailWrapper history={history}/>
      case 'starshipsWrapper':
        return <StarshipsDetailWrapper history={history}/>
      case 'vehiclesWrapper':
        return <VehiclesDetailWrapper history={history}/>
      case 'speciesWrapper':
        return <SpeciesDetailWrapper history={history}/>
      default:
        return <Spin tip="Loading..."/>
    }
  }
  componentWillUnmount(){
    const { content } = this.props;
    this.props.unmountDetailPageAction()
    this.props.resetCurrentContainerDataAction([ content.rightBar])
  }
}

const mapStateToProps = (state) => {
  return {
    config    : state.config,
    detail    : state.detail,
    people    : state.people,
    films     : state.films,
    planets   : state.planets,
    starships : state.starships,
    vehicles  : state.vehicles,
    species   : state.species
  }
}

const mapActionsToProps = {
  resetCurrentContainerDataAction : resetCurrentContainerData,
  getDataContainerPageAction : getDataContainerPage,
  unmountDetailPageAction : unmountDetailPage,
  getDetailIndexForceRefreshAction : getDetailIndexForceRefresh
}

export default withRouter(connect(mapStateToProps,mapActionsToProps)(ChildContainer))