import Loadable from 'react-loadable';
import React from 'react';
export const loaderContainer = (key) => {
  return Loadable({
    loader: () => key,
    loading() {
      return <div>Loading...</div>
    }
  });
}