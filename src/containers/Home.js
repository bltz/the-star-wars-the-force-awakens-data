import React, { Component } from "react";
import { connect } from "react-redux";
import notification from 'antd/lib/notification'
import { getDataContainerPage, resetCurrentContainerData } from '../redux/actions/fetchDataAction';
import CardBoxNonInfiniteLoad from '../components/CardBoxNonInfiniteLoad';
import MainParentContainer from '../layouts/MainParentContainer';
import { responsive } from '../helper';
import { unmountDetailPage } from '../redux/actions/detailAction';

const homeRequestIndex = [
  { type : 'plural', key : 'people', page : 1, pushData : false },
  { type : 'plural', key : 'films', page : 1, pushData : false },
  { type : 'plural', key : 'planets', page : 1, pushData : false },
  { type : 'plural', key : 'starships', page : 1, pushData : false },
  { type : 'plural', key : 'vehicles', page : 1, pushData : false },
  { type : 'plural', key : 'species', page : 1, pushData : false }
]

class Home extends Component {
  constructor(props){
    super(props)
  }
  UNSAFE_componentWillMount(){
    this.props.unmountDetailPageAction()
    return this.props.resetCurrentContainerDataAction( homeRequestIndex )
  }
  componentDidMount(){
    return this.props.getDataContainerPageAction( homeRequestIndex )
  }
  render(){
    const { currentSize : { width, height }, people, films, planets, starships, vehicles, species } = this.props;
    return(
      <MainParentContainer
        history={this.props.history}
        width={ width }
        height={ height }
        leftBar={{
          contentIndicator : 'films',
          requestListLength : 6,
          type: 'page',
          active : width <= responsive.mobileSizeLimit ? false : true
        }}
        rightBar={{
          contentIndicator : 'planets',
          requestListLength : 5,
          type: 'page',
          active : width <= responsive.mobileSizeLimit ? true : true,
          advOptions:{
            source : 'https://tpc.googlesyndication.com/simgad/14921153086685689129',
            altText : 'i dare',
            href : 'https://www.youtube.com/watch?v=wd2nyEG0-vw',
            rel : 'nofollow',
            footer : {
              show : true
            }
          }
        }}
      >
        {
          [ { state : people, indicator : 'people' },
            { state : starships, indicator : 'starships' },
            { state : vehicles, indicator : 'vehicles' },
            { state : species, indicator : 'species' }
          ].map((key,i) => {
            return <CardBoxNonInfiniteLoad
                      history={this.props.history}
                      requestListLength={10}
                      type={'page'}
                      key={i}
                      initialState={key.state}
                      indicator={key.indicator}
                      numberOfListSkeleton={10}
                      affix={{
                        header : true,
                        wraper : false
                      }}
                      title={key.indicator}
                    />
          })
        }
      </MainParentContainer>
    )
  }
  componentWillUnmount(){
    return this.props.resetCurrentContainerDataAction( homeRequestIndex )
  }
}

const mapStateToProps = (state) => {
  return {
    config    : state.config,
    people    : state.people,
    films     : state.films,
    planets   : state.planets,
    starships : state.starships,
    vehicles  : state.vehicles,
    species   : state.species
  }
}

const mapActionsToProps = {
  resetCurrentContainerDataAction : resetCurrentContainerData,
  getDataContainerPageAction : getDataContainerPage,
  unmountDetailPageAction : unmountDetailPage
}

export default connect(mapStateToProps, mapActionsToProps)(Home);