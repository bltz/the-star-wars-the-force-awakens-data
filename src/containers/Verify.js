import React, { Component } from 'react';
import { connect } from "react-redux";
import Input from 'antd/lib/input';
import Row from 'antd/lib/row';
import Col from 'antd/lib/col';
import notification from 'antd/lib/notification'
import logoVert from '../../public/assets/images/sw_logo_stacked@2x-52b4f6d33087.png'
import logoIco from '../../public/assets/images/droid-helmet-soldier-star-wars-512.png';
import { BackgroundImage } from '../components/backgroundImage';
import { generateUserVerify } from '../redux/actions/userAction';
const { Search } = Input;
const openNotification = (placement,why) => {
  switch(why){
    case 'warning':
      return notification.warning({
        message: `Notification`,
        description:
          'Hii.. Please Input Your nick name !!!.',
        placement,
        style : {
          marginLeft : 20
        }
      });
  }
};

class Verify extends Component{
  constructor(){
    super()
    this.state = {
      _loading : false
    }
  }
  render(){
    return (
      <Row style={{ height : '550px'}}>
        <Col span={12} offset={6} style={{ display : 'flex', justifyContent : 'center', alignItems : 'center', height : '100%'}}>
           <div style={{ flexDirection : 'column', display : 'flex'}}>
            <div className="col-wraper" style={{ marginBottom : '30px'}}>
              <BackgroundImage options={{ invert : this.props.config.mode === 'dark' ? false : true ,to : '/', size : '55px', height: '55px', width: '55px', source : logoIco, title : 'Star Wars Logo'}}/>
              <BackgroundImage options={{ invert : this.props.config.mode === 'dark' ? false : true ,to : '/', size : '80px', height: '40px', width: '80px', source : logoVert, title : 'Star Wars Logo'}}/>
            </div>
            <Search
            placeholder="Input Name"
            enterButton="submit"
            size="large"
            onSearch={value => {
              this.setState({ _loading : true },() => {
                return value === '' 
                ? this.setState({ _loading : false },() =>  openNotification('topRight','warning')) 
                : this.props.generateUserVerifyAction(
                  value,
                  ((user) => user.valid && this.props.history.push('/'))
                )
              })
            }}
            loading={this.state._loading} 
          />
           </div>
        </Col>
      </Row>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    config : state.config
  }
}

const mapActionsToProps = {
  generateUserVerifyAction : generateUserVerify
}

export default connect(mapStateToProps,mapActionsToProps)(Verify);