import React from 'react';

class Footer extends React.Component{
  render(){
    return(
      <div>
        <p style={{ fontSize : '11px', textAlign : 'left' }}>
          All the data is accessible through HTTP <a target="_blank" rel={'follow'} href={'https://swapi.co/'}>SWAPI</a> API. 
          Read this <a target="_blank" rel={'follow'} href={'https://swapi.co/documentation'}>documentation</a> if you want to get started. 
          😀This Web Application Development Technology stack using JavaScript library <a target="_blank" rel={'follow'} href={'https://reactjs.org/'}>React Js v16.12.0</a>, State Management <a target="_blank" rel={'follow'} href={'https://redux.js.org/'}>Redux v4.0.4</a>, UI design language <a target="_blank" rel={'follow'} href={'https://ant.design/'}>Antdesign v3.26.0</a>, JavaScript compiler <a target="_blank" rel={'follow'} href={'https://ant.design/'}>Babel v7.7.4</a> and Module bundler <a target="_blank" rel={'follow'} href={'https://webpack.js.org/'}>Webpack v4.41.2</a>.
        </p>
      </div>
    )
  }
}

export default Footer