import React from 'react';
import { connect } from "react-redux";
import { NavLink } from 'react-router-dom';
import { menus } from '../routes/menus';
import Row from 'antd/lib/row';
import Avatar from 'antd/lib/avatar';
import Popover from 'antd/lib/popover';
import Col from 'antd/lib/col';
import Input from 'antd/lib/input';
import Switch from 'antd/lib/switch';
import Icon from 'antd/lib/icon';
import Button from 'antd/lib/button';
import Drawer from 'antd/lib/drawer';
import Affix from 'antd/lib/affix';
import { responsive, stringCapital } from '../helper';
import { BackgroundImage } from './backgroundImage'
import logoHorz from '../../public/assets/images/sw_logo_horiz@2x-f98247cb30aa.png'
import logoVert from '../../public/assets/images/sw_logo_stacked@2x-52b4f6d33087.png'
import logoIco from '../../public/assets/images/droid-helmet-soldier-star-wars-512.png'
import * as ConfigActions from '../redux/actions/configAction';
import { exportVerifyData } from '../redux/actions/userAction';
const { Search } = Input;
const parentPathCheck = (val, current) => ( match, location ) => {
  return current.lastIndexOf(val.initial.replace(/[^\w\s]/gi, '').toLowerCase()) === 1 ? true : current[1] === '' && val.initial.replace(/[^\w\s]/gi, '').toLowerCase() === 'home' ? true : false
}

class Navigation extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      drawerOpen : false
    }
  }
  render(){
    const { config, height, width } = this.props;
    return(
      <Affix offsetTop={0}>
        {/* <BackTop /> */}
        <div style={{ backgroundColor : config.theme.navBgColor }} className="nav-menu-wraper box-shadow">
          { width <= responsive.mobileSizeLimit ? this.menuMobile(height, width,config) : this.menuWeb(height, config,width) }
        </div>
      </Affix>
    )
  }
  menuMobile(height,width,config){
    return(
      <Row type="flex" justify="space-between" align="middle" style={{ height : '100%' }}>
        <Col span={5}>
          <div className="col-wraper">
            <Button type="link" onClick={() => this.setState({ drawerOpen : true })}>
              <Icon type="menu" style={{ fontSize : '25px'}}/>
            </Button>
          </div>
        </Col>
        <Col span={14}>
          <div className="col-wraper">
            <BackgroundImage options={{ invert : this.props.config.mode === 'dark' ? false : true ,to : '/', size : '35px', height: '35px', width: '35px', source : logoIco, title : 'Star Wars Logo'}}/>
            <BackgroundImage options={{ invert : this.props.config.mode === 'dark' ? false : true ,to : '/', size : '70px', height: '30px', width: '70px', source : logoVert, title : 'Star Wars Logo'}}/>
            {/* <BackgroundImage options={{ invert : this.props.config.mode === 'dark' ? false : true , to : '/', size : '210px', height: '20px', width: '210px', source : logoHorz, title : 'Star Wars Logo'}}/> */}
          </div>
        </Col>
        <Col span={5}>
          <div className="col-wraper">
          <Switch onChange={(checked) => this.onChange(checked)} checkedChildren="light" unCheckedChildren="Dark" defaultChecked />
          </div>
        </Col>
        <Drawer
          width={width < 500 ? width-100 : width-200}
          placement="right"
          closable={true}
          onClose={() => this.setState({ drawerOpen : false })}
          visible={this.state.drawerOpen}
          drawerStyle={{ backgroundColor : this.props.config.mode === 'dark' ? '#2b2b2b' : 'white'}}
        >
          <nav>
            <ul style={{ flexDirection : 'column', }}>
              { menus.map((menu,index) => {
                return menu.type === 'parent' &&  menu.private &&
                      <li key={index} style={{marginBottom : '20px'}}>
                        <NavLink 
                          exact 
                          isActive={parentPathCheck(menu,window.location.pathname.split('/'))} 
                          activeStyle={{ fontWeight: "bold", color: "#2598fd" }} 
                          to={menu.path}  
                          style={{ color : config.theme.color, fontWeight : this.props.config.mode === 'dark' ? 900 : 500 }}>
                            {stringCapital(menu.initial)}
                        </NavLink>
                      </li>
              }) 
              }
            </ul>
          </nav>
          <div style={{
            display : 'flex',
            justifyContent : 'flex-end',
            alignItems : 'flex-end',
            height : `${(height/1.9)}px`
          }}>
            <Avatar size="large" style={{ color: '#fff', backgroundColor: '#87d068', marginRight : '10px' }}>{this.props.user.name}</Avatar>
            <div onClick={() => this.props.exportVerifyDataAction(() => setTimeout(() => console.clear(),250)) }>
              <Avatar size="large" style={{ cursor: 'pointer', color: '#fff', backgroundColor: '#f56a00', marginRight : '10px' }}>logout</Avatar>
            </div>
          </div>
        </Drawer>
      </Row>
    )
  }
  
  menuWeb(height,config,width){
    return(
      <Row type="flex" justify="space-between" align="middle" style={{ height : '100%' }}>
        <Col span={width <= responsive.mobileSizeLimit ? 22 : 16} offset={width <= responsive.mobileSizeLimit ? 1 : 4}>
          <Col span={15} style={{ flexDirection : 'row', display : 'flex'}}>
            <div className="col-wraper " style={{ }}>
              <BackgroundImage options={{ invert : this.props.config.mode === 'dark' ? false : true ,to : '/', size : '35px', height: '35px', width: '35px', source : logoIco, title : 'Star Wars Logo'}}/>
              <BackgroundImage options={{ invert : this.props.config.mode === 'dark' ? false : true ,to : '/', size : '70px', height: '30px', width: '70px', source : logoVert, title : 'Star Wars Logo'}}/>
            </div>
            <div className="col-wraper" style={{ }}>
              <nav>
                <ul className="menu">
                { menus.map((menu,index) => {
                  return menu.type === 'parent' && menu.private  ? 
                        <li key={index} style={{ marginRight : '15px' }}>
                          <NavLink 
                            exact 
                            isActive={parentPathCheck(menu,window.location.pathname.split('/'))} 
                            activeStyle={{ fontWeight: "bold", color: "#2598fd" }} 
                            to={menu.path}  
                            style={{ color : config.theme.color, fontWeight : this.props.config.mode === 'dark' ? 900 : 500 }}>
                              {stringCapital(menu.initial)}
                          </NavLink>
                        </li> : null
                }) 
                }
                </ul>
              </nav>
            </div>
          </Col>
          <Col span={9}>
            <div className="col-wraper" style={{ justifyContent : 'flex-end'}}>
            {/* <Search
              placeholder="input search text"
              onSearch={value => console.log(value)}
              style={{ width: width/6, marginRight: '20px', backgroundColor: this.props.config.mode === 'dark' ? '#282828' : 'white', border : this.props.config.mode === 'dark' ? '1px solid #2d2d2d' : '1px solid #d9d9d9' , borderRadius: '4px',}}
            /> */}
            <Avatar size="large" style={{ color: '#fff', backgroundColor: '#87d068', marginRight : '10px' }}>{this.props.user.name}</Avatar>
            <div onClick={() => this.props.exportVerifyDataAction(() => setTimeout(() => console.clear(),250)) }>
              <Avatar size="large" style={{ cursor: 'pointer', color: '#fff', backgroundColor: '#f56a00', marginRight : '10px' }}>logout</Avatar>
            </div>
            <Switch onChange={(checked) => this.onChange(checked)} checkedChildren="light" unCheckedChildren="Dark" defaultChecked />
            </div>
          </Col>
        </Col>
      </Row>
    )
  }
  onChange(checked) {
    const { changeThemeAction } = this.props;
    return changeThemeAction(!checked ? 'light' : 'dark')
  }
}

function mapStateToProps(state) {
  return {
    config : state.config,
    user : state.user
  }
}

const mapActionsToProps = {
  changeThemeAction : ConfigActions.changeTheme,
  exportVerifyDataAction : exportVerifyData
}

export default connect(mapStateToProps,mapActionsToProps)(Navigation);