import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchDataSingularLoop } from '../../api/endpoints';
import TagList from './TagList'
import moment from 'moment';
import 'moment/locale/id';
import { updatedetailWraper } from '../../redux/actions/detailAction'

class FilmsDetailWrapper extends Component{
  constructor(props){
    super(props)
    this.state = {
      _people    : new Array,
      _planets   : new Array,
      _starships : new Array,
      _vehicles  : new Array,
      _species   : new Array
    }
  }
  componentDidMount(){
    return this.getFilmPartialData();
  }
  async getFilmPartialData(){
    const { detail : { body } } = this.props;
    await body.characters !== undefined && fetchDataSingularLoop(
      body.characters,
      ((res) => this.setState({ _people : this.state._people.concat({ _name : res.data.name, _url : res.data.url })}))
    )
    await body.planets !== undefined && fetchDataSingularLoop(
      body.planets,
      ((res) => this.setState({ _planets : this.state._planets.concat({ _name : res.data.name, _url : res.data.url })}))
    )
    await body.starships !== undefined && fetchDataSingularLoop(
      body.starships,
      ((res) => this.setState({ _starships : this.state._starships.concat({ _name : res.data.name, _url : res.data.url })}))
    )
    await body.vehicles !== undefined && fetchDataSingularLoop(
      body.vehicles,
      ((res) => this.setState({ _vehicles : this.state._vehicles.concat({ _name : res.data.name, _url : res.data.url })}))
    )
    await body.species !== undefined && fetchDataSingularLoop(
      body.species,
      ((res) => this.setState({ _species : this.state._species.concat({ _name : res.data.name, _url : res.data.url })}))
    )
  }
  render(){
    const { updatedetailWraperAction, detail : { body }, history, config } = this.props;
    return(
      <div style={{ width : '100%' }}>
        <h1 style={{ color : config.theme.color, marginBottom: '-2px', }}>{body.title}</h1>
        <div style={{ display : 'flex', flexDirection : 'column', marginBottom : '15px', }}>
          <div style={{ display : 'flex', flexDirection : 'row', flexWrap : 'wrap', }}>
            <h5 style={{ color : config.theme.color , marginRight : '10px', marginBottom: '-2px', }}>Director : {body.director},</h5>
            <h5 style={{ color : config.theme.color , marginRight : '10px', marginBottom: '-2px', }}>Producer : {body.producer},</h5>
          </div>
          <h5 style={{ color : config.theme.color ,marginBottom: '-2px', }}>Created in {moment(body.created).format('LLLL')}</h5>
        </div>
        <p style={{ fontSize : 16}}>
          {`A film release date in ${body.release_date}. ${body.opening_crawl}`}
        </p>
        <TagList
          title={'characters'}
          options={{ indicator : 'people', mode : config.mode, history : history }}
          datas={this.state._people}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'planet', mode : config.mode, history : history }}
          datas={this.state._planets}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'starship', mode : config.mode, history : history }}
          datas={this.state._starships}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'vehicle', mode : config.mode, history : history }}
          datas={this.state._vehicles}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'species', mode : config.mode, history : history }}
          datas={this.state._species}
          action={updatedetailWraperAction}
        />
        <div style={{ display : 'flex', justifyContent : 'flex-end', marginTop : '50px' }}>
          <h6 style={{ color : '#9E9E9E' }}>Last modified : {moment(body.edited).format('LLLL')}</h6>
        </div>
      </div>
    )
  }
  componentWillUnmount(){
    this.setState({})
  }
}

const mapStateToProps = (state) => {
  return {
    config    : state.config,
    detail    : state.detail,
  }
}

const mapActionsToProps = {
  updatedetailWraperAction : updatedetailWraper
}

export default connect(mapStateToProps,mapActionsToProps)(FilmsDetailWrapper)