import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchDataSingularLoop } from '../../api/endpoints';
import TagList from './TagList'
import moment from 'moment';
import 'moment/locale/id';
import { updatedetailWraper } from '../../redux/actions/detailAction'

class PeopleDetailWrapper extends Component{
  constructor(props){
    super(props)
    this.state = {
      _planets : new Array,
      _films : new Array,
      _vehicles : new Array,
      _starships : new Array
    }
  }
  componentDidMount(){
    return this.getPersonPartialData();
  }
  async getPersonPartialData(){
    const { detail : { body } } = this.props;
    await body.homeworld !== undefined && fetchDataSingularLoop(
      [body.homeworld],
      ((res) => this.setState({ _planets : this.state._planets.concat({ _name : res.data.name, _url : res.data.url })}))
    )
    await body.films !== undefined && fetchDataSingularLoop(
      body.films,
      ((res) => this.setState({ _films : this.state._films.concat({_name : res.data.title, _url : res.data.url })}))
    )
    await body.vehicles !== undefined && fetchDataSingularLoop(
      body.vehicles,
      ((res) => this.setState({ _vehicles : this.state._vehicles.concat({_name : res.data.name, _url : res.data.url })}))
    )
    await body.starships !== undefined && fetchDataSingularLoop(
      body.starships,
      ((res) => this.setState({ _starships : this.state._starships.concat({_name : res.data.name, _url : res.data.url })}))
    )
  }
  render(){
    const { updatedetailWraperAction, detail : { body }, history, config } = this.props;
    return(
      <div style={{ width : '100%' }}>
        <h1 style={{ color : config.theme.color }}>{body.name}</h1>
        <div style={{ display : 'flex', flexDirection : 'row', marginTop: '-15px'}}>
          <h5 style={{ color : config.theme.color }}>Created : {moment(body.created).format('LLLL')}</h5>
        </div>
        <div style={{ display : 'flex', flexDirection : 'row', marginTop : '10px'}}>
          <p style={{ fontSize : 16}}>{`${body.name.split(' ')[0]} is ${body.gender}, mass body ${body.mass} birth in year ${body.birth_year} hair color ${body.hair_color}, eye color ${body.eye_color}, and skin color ${body.skin_color}.`}</p>
        </div>
        <TagList
        title={'homeworld'}
          options={{ indicator : 'planet', mode : config.mode, history : history }}
          datas={this.state._planets}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'film', mode : config.mode, history : history }}
          datas={this.state._films}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'vehicle', mode : config.mode, history : history }}
          datas={this.state._vehicles}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'starship', mode : config.mode, history : history }}
          datas={this.state._starships}
          action={updatedetailWraperAction}
        />
        <div style={{ display : 'flex', justifyContent : 'flex-end', marginTop : '50px' }}>
          <h6 style={{ color : '#9E9E9E' }}>Last modified : {moment(body.edited).format('LLLL')}</h6>
        </div>
      </div>
    )
  }
  componentWillUnmount(){
    this.setState({})
  }
}

const mapStateToProps = (state) => {
  return {
    config : state.config,
    detail : state.detail,
  }
}

const mapActionsToProps = {
  updatedetailWraperAction : updatedetailWraper
}

export default connect(mapStateToProps, mapActionsToProps)(PeopleDetailWrapper)