import React, { Component } from "react";
import { connect } from "react-redux";
import Tag from 'antd/lib/tag';
import { stringCapital } from  '../../helper'
import slugify from 'slugify';

class TagList extends Component{
  render(){
    const { options, datas, action } = this.props
    return(
      <div>
        {
          datas.length === 0
          ? null
          : <div style={{ display : 'flex', flexDirection : 'row', marginBottom : '5px' }}>
              <div style={{ display : 'flex', flexDirection : 'row', flexWrap: 'nowrap' }}>
                {
                  this.props.title
                  ? <p style={{ margin : 0}}>{`${stringCapital(this.props.title)}`}</p>
                  : <p style={{ margin : 0}}>{`${stringCapital(options.indicator)}${options.indicator === 'people' || options.indicator === 'species' ? '' : 's'}`}</p>
                }
                <p style={{ margin : 0, marginLeft : '5px'}}> :</p>
              </div>
              <div style={{ marginLeft : 15, display : 'flex', flexDirection : 'row', flexWrap : 'wrap' }}>
                {
                  datas.map((iterate,iterateIndex) => {
                    return(
                      <div key={`${options.indicator}::${iterateIndex}`} style={{ cursor: 'pointer'}} onClick={() => {
                        options.history.push({
                          pathname : `/${options.indicator}${options.indicator === 'people' || options.indicator === 'species' ? '' : 's'}/${slugify(iterate._name, { replacement: '-', remove: true, lower: true })}`, 
                          search : `?id=${iterate._url.split('/')[iterate._url.split('/').length-2]}` 
                        })
                        return action && action(`${options.indicator}${options.indicator === 'people' || options.indicator === 'species' ? '' : 's'}`, { url : iterate._url })
                      }}>
                        <Tag color={options.mode === 'dark' ? "#1890ff" : 'blue'} style={{ cursor: 'pointer', marginRight : '10px', marginBottom : '10px'   }}>
                          {iterate._name}
                        </Tag>
                      </div>
                    )
                  })
                }
              </div>
            </div>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    config : state.config,
    detail : state.detail,
  }
}

export default connect(mapStateToProps)(TagList)