import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchDataSingularLoop } from '../../api/endpoints';
import TagList from './TagList'
import moment from 'moment';
import 'moment/locale/id';
import { updatedetailWraper } from '../../redux/actions/detailAction'

class StarshipsDetailWrapper extends Component{
  constructor(props){
    super(props)
    this.state = {
      _people : new Array,
      _films : new Array,
    }
  }
  componentDidMount(){
    return this.getStarshipsPartialData();
  }
  async getStarshipsPartialData(){
    const { detail : { body } } = this.props;
    await body.pilots !== undefined && fetchDataSingularLoop(
      body.pilots,
      ((res) => this.setState({ _people : this.state._people.concat({ _name : res.data.name, _url : res.data.url })}))
    )
    await body.films !== undefined && fetchDataSingularLoop(
      body.films,
      ((res) => this.setState({ _films : this.state._films.concat({_name : res.data.title, _url : res.data.url })}))
    )
  }
  render(){
    const { updatedetailWraperAction, detail : { body }, history, config } = this.props;
    return(
      <div style={{ width : '100%' }}>
        <h1 style={{ color : config.theme.color }}>{body.name}</h1>
        <div style={{ display : 'flex', flexDirection : 'row', marginTop: '-15px'}}>
          <h5 style={{ color : config.theme.color }}>Created : {moment(body.created).format('LLLL')}</h5>
        </div>
        <div style={{ display : 'flex', flexDirection : 'row', marginTop : '10px'}}>
          <p style={{ fontSize : 16}}>
            {`${body.model} is starship model - ${body.max_atmosphering_speed} top speed an ${body.starship_class} class, manufacture ${body.manufacturer}, cost in credits is ${body.cost_in_credits}, has length ${body.length}, total crew ${body.crew} ${body.passengers}, ${body.cargo_capacity} cargo capacity, ${body.consumables} consumables, ${body.hyperdrive_rating} hyperdrive rating, MGLT is ${body.MGLT} .`}
          </p>
        </div>
        <TagList
          title={'pilots'}
          options={{ indicator : 'people', mode : config.mode, history : history }}
          datas={this.state._people}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'film', mode : config.mode, history : history }}
          datas={this.state._films}
          action={updatedetailWraperAction}
        />
        <div style={{ display : 'flex', justifyContent : 'flex-end', marginTop : '50px' }}>
          <h6 style={{ color : '#9E9E9E' }}>Last modified : {moment(body.edited).format('LLLL')}</h6>
        </div>
      </div>
    )
  }
  componentWillUnmount(){
    this.setState({})
  }
}

const mapStateToProps = (state) => {
  return {
    config : state.config,
    detail : state.detail,
  }
}

const mapActionsToProps = {
  updatedetailWraperAction : updatedetailWraper
}

export default connect(mapStateToProps,mapActionsToProps)(StarshipsDetailWrapper)