import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchDataSingularLoop } from '../../api/endpoints';
import TagList from './TagList'
import moment from 'moment';
import 'moment/locale/id';
import { updatedetailWraper } from '../../redux/actions/detailAction'

class SpeciesDetailWrapper extends Component{
  constructor(props){
    super(props)
    this.state = {
      _planets : new Array,
      _people : new Array,
      _films : new Array,
    }
  }
  componentDidMount(){
    return this.getSpeciesPartialData();
  }
  async getSpeciesPartialData(){
    const { detail : { body } } = this.props;
    await body.homeworld !== undefined && fetchDataSingularLoop(
      [body.homeworld],
      ((res) => this.setState({ _planets : this.state._planets.concat({ _name : res.data.name, _url : res.data.url })}))
    )
    await body.people !== undefined && fetchDataSingularLoop(
      body.people,
      ((res) => this.setState({ _people : this.state._people.concat({ _name : res.data.name, _url : res.data.url })}))
    )
    await body.films !== undefined && fetchDataSingularLoop(
      body.films,
      ((res) => this.setState({ _films : this.state._films.concat({_name : res.data.title, _url : res.data.url })}))
    )
  }
  render(){
    const { updatedetailWraperAction, detail : { body }, history, config } = this.props;
    return(
      <div style={{ width : '100%' }}>
        <h1 style={{ color : config.theme.color }}>{body.name}</h1>
        <div style={{ display : 'flex', flexDirection : 'row', marginTop: '-15px'}}>
          <h5 style={{ color : config.theme.color }}>Created : {moment(body.created).format('LLLL')}</h5>
        </div>
        <div style={{ display : 'flex', flexDirection : 'row', marginTop : '10px'}}>
          <p style={{ fontSize : 16}}>
            {`${body.name.split("'")[0]} is ${body.classification} classification, speak ${body.language} language, design by ${body.designation} have a average height ${body.average_height} and ${body.average_lifespan} average lifespan, ${body.skin_colors} is skin colors, ${body.hair_colors} is hair colors, ${body.eye_colors} is eye colors.`}
          </p>
        </div>
        <TagList
          title={'homeworld'}
          options={{ indicator : 'planet', mode : config.mode, history : history }}
          datas={this.state._planets}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'people', mode : config.mode, history : history }}
          datas={this.state._people}
          action={updatedetailWraperAction}
        />
        <TagList
          options={{ indicator : 'film', mode : config.mode, history : history }}
          datas={this.state._films}
          action={updatedetailWraperAction}
        />
        <div style={{ display : 'flex', justifyContent : 'flex-end', marginTop : '50px' }}>
          <h6 style={{ color : '#9E9E9E' }}>Last modified : {moment(body.edited).format('LLLL')}</h6>
        </div>
      </div>
    )
  }
  componentWillUnmount(){
    this.setState({})
  }
}

const mapStateToProps = (state) => {
  return {
    config : state.config,
    detail : state.detail,
  }
}

const mapActionsToProps = {
  updatedetailWraperAction : updatedetailWraper
}

export default connect(mapStateToProps,mapActionsToProps)(SpeciesDetailWrapper)