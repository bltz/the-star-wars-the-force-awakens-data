import FilmsDetailWrapper     from './FilmsDetailWrapper';
import PeopleDetailWrapper    from './PeopleDetailWrapper';
import PlanetsDetailWrapper   from './PlanetsDetailWrapper';
import SpeciesDetailWrapper   from './SpeciesDetailWrapper';
import StarshipsDetailWrapper from './StarshipsDetailWrapper';
import VehiclesDetailWrapper  from './VehiclesDetailWrapper';
export {
  FilmsDetailWrapper,
  PeopleDetailWrapper,
  PlanetsDetailWrapper,
  SpeciesDetailWrapper,
  StarshipsDetailWrapper,
  VehiclesDetailWrapper
}