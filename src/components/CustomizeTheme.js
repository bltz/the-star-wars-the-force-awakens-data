import React from 'react';
import DynamicAntdTheme from 'dynamic-antd-theme';
export const CustomizeTheme = () => {
  return(
    <div style={{ display : 'none'}}>
      <DynamicAntdTheme primaryColor='#f5a623' />
    </div>
  )
}