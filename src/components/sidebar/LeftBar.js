import React, { Component } from "react";
import Col from 'antd/lib/col';
import CardBoxNonInfiniteLoad from '../CardBoxNonInfiniteLoad';
// import AdvertiseBox300x300 from '../AdvertiseBox300x300';

class LeftBar extends Component{
  render(){
    const { col, topBox } = this.props
    return(
      <Col span={col.span}>
        <CardBoxNonInfiniteLoad
        history={this.props.history}
          type={topBox.type}
          requestListLength={topBox.requestListLength}
          initialState={topBox.initialState}
          indicator={topBox.indicator}
          // numberOfListSkeleton={topBox.numberOfListSkeleton}
          affix={{
            header :  topBox.affix.header,
            wraper : topBox.affix.wraper
          }}
          title={topBox.title}
        />
      </Col>
    )
  }
}

export default LeftBar