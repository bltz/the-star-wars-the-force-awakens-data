import React, { Component } from "react";
import Col from 'antd/lib/col';
import CardBoxNonInfiniteLoad from '../CardBoxNonInfiniteLoad';
import AdvertiseBox300x300 from '../AdvertiseBox300x300';

class RightBar extends Component{
  render(){
    const { col, topBox, advBox } = this.props;
    return(
      <Col span={col.span}>
        <CardBoxNonInfiniteLoad
          history={this.props.history}
          type={topBox.type}
          requestListLength={topBox.requestListLength}
          initialState={topBox.initialState}
          indicator={topBox.indicator}
          affix={{
            header :  topBox.affix.header,
            wraper : topBox.affix.wraper
          }}
          title={topBox.title}
        />
        <AdvertiseBox300x300
          affix={advBox.affix}
          responsiveAccurate={advBox.responsiveAccurate}
          options={advBox.options}
          footer={advBox.footer}
        />
      </Col>
    )
  }
}

export default RightBar