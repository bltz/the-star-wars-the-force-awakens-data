import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { ListSkeleton } from './contentLoader'
import Affix from 'antd/lib/affix';
import Spin from 'antd/lib/spin'
import { responsive } from '../helper';
import {List} from './list/List';
import { spinnerON, spinnerOFF } from '../redux/actions/configAction';
import { getDetailIndex } from '../redux/actions/detailAction'
const ListRender = List()

class CardBoxNonInfiniteLoad extends Component{
  render(){
    const { affix } = this.props;
    return affix && affix.wraper ? <Affix offsetTop={80}>{this.contentRender()}</Affix> : this.contentRender()
  }
  contentRender(){
    const { type, requestListLength, initialState, config, affix, numberOfListSkeleton, indicator, history, spinnerONaction, spinnerOFFaction, getDetailIndexAction } = this.props;
    // console.log(this.props,'this.props')
    return(
      <div style={{ flexDirection : 'column', borderColor : config.theme.borderColor, marginBottom : responsive.marginBottom }} className="box-border">
        { affix && affix.header ? <Affix offsetTop={80}>{ this.headerRender() }</Affix> : this.headerRender() }
        <div style={{ padding : '15px', backgroundColor : config.theme.accentBg }}>
          {
            initialState.load === 'pending'
            ? <ListSkeleton 
                body={{ 
                  paragraph : {
                    title : true, 
                    rows : 1 , 
                  },
                  avatar : {
                    show : false,
                    size : 'large',
                    shape : 'circle'
                  },
                  active : true,
                  numberOfList : requestListLength 
                }}
              />
             : <div>
                {
                  initialState.datas.map((data,dataIndex) => {
                    return(
                      <div key={dataIndex}>
                        { 
                          ListRender[indicator](
                            config, data, dataIndex, requestListLength, type, history,
                            { spinnerONaction, spinnerOFFaction, getDetailIndexAction },
                            initialState.datas.length
                          ) 
                        }
                      </div>
                    )
                  })
                }
               </div>
          }
        </div>
      </div>
    )
  }
  headerRender(){
    const { initialState, config, title, indicator } = this.props;
    return(
      <div 
        style={{
          backgroundColor : config.theme.headIndex,
          borderTopLeftRadius : 5,
          borderTopRightRadius : 5, 
          display : 'flex',
          flexDirection : 'row',
          flexWrap : 'nowrap',
          alignItems : 'center',
          padding : '15px',
          justifyContent : 'space-between'
        }}
      >
        <p style={{ marginBottom : 0, color : config.theme.color, fontSize : 16, fontWeight : 500 }}>
          {`${title.toUpperCase()}`}
        </p>
        {
          initialState.count === 0 ? <Spin size="small" /> : <Link to={indicator ? `/${indicator}` : '/'} title={`getAll${title}`} style={{ fontSize : '12px' }}>{`${initialState.count !== 0 ? initialState.count : ''} index`}</Link>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    config : state.config
  }
}

const mapActionsToProps = {
  spinnerONaction : spinnerON,
  spinnerOFFaction : spinnerOFF,
  getDetailIndexAction : getDetailIndex
}

export default connect(mapStateToProps,mapActionsToProps)(CardBoxNonInfiniteLoad)