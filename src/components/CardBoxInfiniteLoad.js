import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';
import Affix from 'antd/lib/affix';
import Spin from 'antd/lib/spin';
import Icon from 'antd/lib/icon'
import { responsive } from '../helper';

class CardBoxInfiniteLoad extends Component{
  render(){
    const { affix } = this.props;
    return affix && affix.wraper ? <Affix offsetTop={80}>{this.contentRender()}</Affix> : this.contentRender()
  }
  contentRender(){
    const { children, config, affix } = this.props;
    return(
      <div style={{ flexDirection : 'column', borderColor : config.theme.borderColor, marginBottom : responsive.marginBottom }} className="box-border">
        { affix && affix.header ? <Affix offsetTop={80}>{ this.headerRender() }</Affix> : this.headerRender() }
        <div style={{ padding : '15px', backgroundColor : config.theme.accentBg }}>
          { children }
        </div>
      </div>
    )
  }
  headerRender(){
    const { config, title, load } = this.props;
    return(
      <div 
        style={{
          backgroundColor : config.theme.headIndex,
          borderTopLeftRadius : 5,
          borderTopRightRadius : 5, 
          display : 'flex',
          flexDirection : 'row',
          flexWrap : 'nowrap',
          alignItems : 'center',
          padding : '15px',
          justifyContent : 'space-between'
        }}
      >
        <p style={{ marginBottom : 0, color : config.theme.color, fontSize : 16, fontWeight : 500 }}>
          {`${title.toUpperCase()}`}
        </p>
        {
          load !== 'infinite_load_finish' ? <Spin size="small" /> : <Icon style={{ fontSize : 17.5 }} type="check-circle"  />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    config : state.config
  }
}

export default connect(mapStateToProps)(CardBoxInfiniteLoad)