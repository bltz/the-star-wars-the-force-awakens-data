
import React from 'react'
import Spin from 'antd/lib/spin'
import slugify from 'slugify'
import moment from 'moment';
import 'moment/locale/id';


export const List = () => {
  const people = ( config, data, dataIndex, requestListLength, type, history, action, totalData ) => {
    return type === 'page' && dataIndex < requestListLength 
           ? peopleChild( config, data, history, action, dataIndex, totalData, requestListLength )
           : requestListLength === null && type === 'infinite' && peopleChild(config, data, history, action, dataIndex, totalData, requestListLength)
      
  }

  const peopleChild = ( config, data, history, action , dataIndex, totalData, requestListLength ) => {
    const listInitial = 'peopleWrapper'
    return (
      <div style={{ marginTop : '10px'}}>
        <div style={{ cursor: 'pointer'}} onClick={() => {
          action.spinnerONaction(
            { show : true, index : dataIndex, wrapper : listInitial },
            (() => action.getDetailIndexAction(
              { title : data.name, url : data.url,  wrapper : listInitial },
              ((status) => status === 200 && history.push({ 
                pathname : `/people/${slugify(data.name, { replacement: '-', remove: true, lower: true })}`, 
                search : `?id=${data.url.split('/')[data.url.split('/').length-2]}` 
              }))
            ))
          )
        }}>
          <div style={{ display : 'flex',lexDirection : 'row', justifyContent : 'space-between' }}>
            <h3 style={{ color : config.theme.color }}>{data.name}</h3>
            {
              config.listSpiner.show && config.listSpiner.wrapper === listInitial 
              && config.listSpiner.index === dataIndex && <Spin size="small" />
            }
          </div>
        </div>
        <h5 style={{ color : '#9E9E9E', marginTop : '-5px', marginBottom : '5px' }}>{`${data.gender} - ${moment(data.created, 'YYYYMMDD').fromNow()}`}</h5>
        {
          totalData === (dataIndex + 1) || requestListLength === (dataIndex + 1) ? null : <div style={{ borderBottom : config.mode === 'dark' ? '1px solid #1c1c1c' : '1px solid #f1f1f1', marginTop : '15px' }}/>
        }
      </div>
    )
  }

  const films = (config, data, dataIndex, requestListLength, type, history, action, totalData) => {
    return type === 'page' && dataIndex < requestListLength 
           ? filmsChild(config, data, history, action, dataIndex, totalData, requestListLength)
           : requestListLength === null && type === 'infinite' && filmsChild(config, data, history, action, dataIndex, totalData, requestListLength)
      
  }

  const filmsChild = (config, data, history, action, dataIndex, totalData, requestListLength) => {
    const listInitial = 'filmsWrapper'
    return (
      <div style={{ marginTop : '10px'}}>
        <div style={{ cursor: 'pointer'}} onClick={() => {
          action.spinnerONaction(
            { show : true, index : dataIndex, wrapper : listInitial },
            (() => action.getDetailIndexAction(
              { title : data.title, url : data.url,  wrapper : listInitial },
              ((status) => status === 200 && history.push({ 
                pathname : `/films/${slugify(data.title, { replacement: '-', remove: true, lower: true })}`,  
                search : `?id=${data.url.split('/')[data.url.split('/').length-2]}` 
              }))
            ))
          )
        }}>
          <div style={{ display : 'flex',lexDirection : 'row', justifyContent : 'space-between' }}>
            <h3 style={{ color : config.theme.color }}>{data.title}</h3>
            {
              config.listSpiner.show && config.listSpiner.wrapper === listInitial 
              && config.listSpiner.index === dataIndex && <Spin size="small" />
            }
          </div>
        </div>
        <h5 style={{ color : '#9E9E9E', marginTop : '-5px', marginBottom : '5px' }}>{`${data.director} - ${moment(data.release_date,'YYYYMMDD').fromNow()}`}</h5>
        {
          totalData === (dataIndex + 1) || requestListLength === (dataIndex + 1) ? null : <div style={{ borderBottom : config.mode === 'dark' ? '1px solid #1c1c1c' : '1px solid #f1f1f1', marginTop : '15px' }}/>
        }
      </div>
    )
  }

  const planets = (config, data, dataIndex, requestListLength, type, history, action, totalData) => {
    return type === 'page' && dataIndex < requestListLength 
           ? planetsChild(config, data, history, action, dataIndex, totalData, requestListLength)
           : requestListLength === null && type === 'infinite' && planetsChild(config, data, history, action, dataIndex, totalData, requestListLength)
  }

  const planetsChild = (config, data, history, action, dataIndex, totalData, requestListLength) => {
    const listInitial = 'planetsWrapper'
    return (
      <div style={{ marginTop : '10px'}}>
        <div style={{ cursor: 'pointer'}} onClick={() => {
          action.spinnerONaction(
            { show : true, index : dataIndex, wrapper : listInitial },
            (() => action.getDetailIndexAction(
              { title : data.name, url : data.url,  wrapper : listInitial },
              ((status) => status === 200 && history.push({ 
                pathname : `/planets/${slugify(data.name, { replacement: '-', remove: true, lower: true })}`,  
                search : `?id=${data.url.split('/')[data.url.split('/').length-2]}` 
              }))
            ))
          )
        }}>
          <div style={{ display : 'flex',lexDirection : 'row', justifyContent : 'space-between' }}>
            <h3 style={{ color : config.theme.color }}>{data.name}</h3>
            {
              config.listSpiner.show && config.listSpiner.wrapper === listInitial 
              && config.listSpiner.index === dataIndex && <Spin size="small" />
            }
          </div>
        </div>
        <h5 style={{ color : '#9E9E9E', marginTop : '-5px', marginBottom : '5px' }}>{`${data.diameter} - ${moment(data.created, 'YYYYMMDD').fromNow()}`}</h5>
        {
          totalData === (dataIndex + 1) || requestListLength === (dataIndex + 1) ? null : <div style={{ borderBottom : config.mode === 'dark' ? '1px solid #1c1c1c' : '1px solid #f1f1f1', marginTop : '15px' }}/>
        }
      </div>
    )
  }

  const starships = (config, data, dataIndex, requestListLength, type, history, action, totalData) => {
    return type === 'page' && dataIndex < requestListLength 
           ? starsChild(config, data, history, action, dataIndex, totalData, requestListLength)
           : requestListLength === null && type === 'infinite' && starsChild(config, data, history, action, dataIndex, totalData, requestListLength)
  }

  const starsChild = (config, data, history, action, dataIndex, totalData, requestListLength) => {
    const listInitial = 'starshipsWrapper'
    return (
      <div style={{ marginTop : '10px'}}>
        <div style={{ cursor: 'pointer', marginBottom : '10px'}} onClick={() => {
          action.spinnerONaction(
            { show : true, index : dataIndex, wrapper : listInitial },
            (() => action.getDetailIndexAction(
              { title : data.name, url : data.url,  wrapper : listInitial },
              ((status) => status === 200 && history.push({ 
                pathname : `/starships/${slugify(data.name,{ replacement: '-', remove: true, lower: true })}`, 
                search : `?id=${data.url.split('/')[data.url.split('/').length-2]}` 
              }))
            ))
          )
        }}>
          <div style={{ display : 'flex',lexDirection : 'row', justifyContent : 'space-between' }}>
            <h3 style={{ color : config.theme.color }}>{data.name}</h3>
            {
              config.listSpiner.show && config.listSpiner.wrapper === listInitial 
              && config.listSpiner.index === dataIndex && <Spin size="small" />
            }
          </div>
        </div>
        <h5 style={{ color : '#9E9E9E', marginTop : '-5px', marginBottom : '5px' }}>{`${data.starship_class} - ${moment(data.created, 'YYYYMMDD').fromNow()}`}</h5>
        {
          totalData === (dataIndex + 1) || requestListLength === (dataIndex + 1) ? null : <div style={{ borderBottom : config.mode === 'dark' ? '1px solid #1c1c1c' : '1px solid #f1f1f1', marginTop : '15px' }}/>
        }
      </div>
    )
  }

  const vehicles = (config, data, dataIndex, requestListLength, type, history, action, totalData) => {
    return type === 'page' && dataIndex < requestListLength 
           ? vehiclesChild(config, data, history, action, dataIndex, totalData, requestListLength)
           : requestListLength === null && type === 'infinite' && vehiclesChild(config, data, history, action, dataIndex, totalData, requestListLength)
  }

  const vehiclesChild = (config, data, history, action, dataIndex, totalData, requestListLength) => {
    const listInitial = 'vehiclesWrapper'
    return (
      <div style={{ marginTop : '10px'}}>
        <div style={{ cursor: 'pointer'}} onClick={() => {
          action.spinnerONaction(
            { show : true, index : dataIndex, wrapper : listInitial },
            (() => action.getDetailIndexAction(
              { title : data.name, url : data.url,  wrapper : listInitial },
              ((status) => status === 200 && history.push({ 
                pathname : `/vehicles/${slugify(data.name,{ replacement: '-', remove: true, lower: true })}`, 
                search : `?id=${data.url.split('/')[data.url.split('/').length-2]}` 
              }))
            ))
          )
        }}>
          <div style={{ display : 'flex',lexDirection : 'row', justifyContent : 'space-between' }}>
            <h3 style={{ color : config.theme.color }}>{data.name}</h3>
            {
              config.listSpiner.show && config.listSpiner.wrapper === listInitial 
              && config.listSpiner.index === dataIndex && <Spin size="small" />
            }
          </div>
        </div>
        <h5 style={{ color : '#9E9E9E', marginTop : '-5px', marginBottom : '5px' }}>{`${data.model} - ${moment(data.created, 'YYYYMMDD').fromNow()}`}</h5>
        {
          totalData === (dataIndex + 1) || requestListLength === (dataIndex + 1)  ? null : <div style={{ borderBottom : config.mode === 'dark' ? '1px solid #1c1c1c' : '1px solid #f1f1f1', marginTop : '15px' }}/>
        }
      </div>
    )
  }

  const species = (config, data, dataIndex, requestListLength, type, history, action, totalData) => {
    return type === 'page' && dataIndex < requestListLength 
           ? speciesChild(config, data, history, action, dataIndex, totalData, requestListLength)
           : requestListLength === null && type === 'infinite' && speciesChild(config, data, history, action, dataIndex, totalData, requestListLength)
  }

  const speciesChild = ( config, data, history, action, dataIndex, totalData, requestListLength ) => {
    const listInitial = 'speciesWrapper'
    return (
      <div style={{ marginTop : '10px'}}>
        <div style={{ cursor: 'pointer'}} onClick={() => {
          action.spinnerONaction(
            { show : true, index : dataIndex, wrapper : listInitial },
            (() => action.getDetailIndexAction(
              { title : data.name, url : data.url,  wrapper : listInitial },
              ((status) => status === 200 && history.push({ 
                pathname : `/species/${slugify(data.name,{ replacement: '-', remove: true, lower: true })}`, 
                search : `?id=${data.url.split('/')[data.url.split('/').length-2]}` 
              }))
            ))
          )
        }}>
          <div style={{ display : 'flex',lexDirection : 'row', justifyContent : 'space-between' }}>
            <h3 style={{ color : config.theme.color }}>{data.name}</h3>
            {
              config.listSpiner.show && config.listSpiner.wrapper === listInitial 
              && config.listSpiner.index === dataIndex && <Spin size="small" />
            }
          </div>
        </div>
        <h5 style={{ color : '#9E9E9E', marginTop : '-5px', marginBottom : '5px' }}>{`${data.designation} - ${moment(data.created, 'YYYYMMDD').fromNow()}`}</h5>
        {
          totalData === (dataIndex + 1) || requestListLength === (dataIndex + 1) ? null : <div style={{ borderBottom : config.mode === 'dark' ? '1px solid #1c1c1c' : '1px solid #f1f1f1', marginTop : '15px' }}/>
        }
      </div>
    )
  }
  

  return {
    people,
    films,
    planets,
    starships,
    vehicles,
    species
  }
}