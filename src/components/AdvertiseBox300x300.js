import React, { Component } from "react";
import { responsive } from '../helper';
import Affix from 'antd/lib/affix';
import Footer from '../components/Footer'

class AdvertiseBox300x300 extends Component{
  render(){
    const { affix } = this.props
    return affix ? <Affix offsetTop={60}>{ this.advertiseRender() }</Affix> : this.advertiseRender()
  }
  advertiseRender(){
    const { options, responsiveAccurate : { height, width }, footer } = this.props
    let wraper = width >= responsive.mobileSizeLimit 
                 ? {  marginBottom : responsive.marginBottom, backgroundColor : 'black', width : '100%' } 
                 : { marginBottom : responsive.marginBottom, backgroundColor : 'black', width : '100%', display : 'flex', justifyContent : 'center'}
    return(
      <div style={{ display : 'flex', flexDirection : 'column', alignItems : 'center', marginTop : '20px' }}>
        <div style={wraper}>
          <a target="_blank" rel={options.rel} href={options.href}>
            <img 
              style={{
                width: width <= responsive.mobileSizeLimit ? '300px' : '100%', 
                height:'auto'
              }}
              alt={options.altText} 
              src={options.source}/>
          </a>
        </div>
        { footer.show && width >= responsive.mobileSizeLimit && <Footer/>}
      </div>
    )
  }
}

export default AdvertiseBox300x300