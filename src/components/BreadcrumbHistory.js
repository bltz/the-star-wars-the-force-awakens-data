import React from 'react';
import Breadcrumb from 'antd/lib/breadcrumb';
import { Link } from 'react-router-dom';
import { stringCapital } from '../helper'

class BreadcrumbHistory extends React.Component{
  componentDidMount(){
    return setTimeout(() => console.clear(),1500)
  }
  render(){
    const { config, parent, current } = this.props
    return(
      <Breadcrumb separator={<a style={{ color : config.theme.color}}>></a>} style={{ display : 'flex' , flexDirection : 'row'}}>
        <Breadcrumb.Item>
          <Link to={'/'} style={{ color : config.theme.color }}>Home</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to={`/${parent}`} style={{ color : config.theme.color }}>{`${stringCapital(parent)}`}</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <h4 style={{ color : '#9E9E9E', fontWeight : 600 }}>{current}</h4>
        </Breadcrumb.Item>
      </Breadcrumb>
    )
  }
}

export default BreadcrumbHistory