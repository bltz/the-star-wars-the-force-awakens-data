import React, { Component } from 'react';
import Skeleton from 'antd/lib/skeleton';
import { minMaxNumberRandom } from '../../helper';

class ListSkeleton extends Component{
  render(){
    const { body : { avatar : { show, size, shape }, paragraph : { title, rows }, active, numberOfList} } = this.props;
    return new Array(numberOfList ? numberOfList : 1).fill(null).map((init,index) => {
      return  <Skeleton 
                key={`skeleton${index}`} 
                size="small" 
                avatar={show ? { size, shape } : false}
                active={ active ? active : true } 
                title={title ? { width : `${minMaxNumberRandom(38,55)}%` } : false }
                paragraph={{ 
                  rows: rows ? rows : 2, 
                  width : `${minMaxNumberRandom(75,100)}%` 
                }}
              />
    })
  }
}

export default ListSkeleton