import React from 'react';
import { Link } from 'react-router-dom';
export const BackgroundImage = (props) => {
  return <Link 
            to={props.options.to} 
            style ={{ 
              backgroundSize: props.options.size, 
              float: 'left',
              height: props.options.height,
              width: props.options.width, 
              backgroundImage: `url(${props.options.source})` ,
              backgroundRepeat : 'no-repeat',
              filter: props.options.invert ? 'invert(1)' : 'none'
            }} 
            title={props.options.title}
          />
}