import * as CONFIG_USER from '../actions/userAction';
// import { mainColors } from '../../helper';

const initialState = {
  name : null,
  valid : false,
  uuid : null
};

export const userReducer = (state=initialState, action) => {
  switch(action.type) {
    case CONFIG_USER.IMPORT_VERIFY_DATA :
      return {
        ...state,
        name : action.payload.name,
        valid : true,
        uuid : action.payload.uuid
      }
    case CONFIG_USER.EXPORT_VERIFY_DATA :
      return {
        name : null,
        valid : false,
        uuid : null
      }
    default:
      return state
  }
}