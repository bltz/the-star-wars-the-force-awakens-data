import * as DETAIL_ACTION from '../actions/detailAction';

const initialState = {
  breadcrumbTitle : '...',
  body : new Object,
  status : 0,
  wrapper : ''
};

export const detailReducer = (state=initialState, action) => {
  switch(action.type) {
    case DETAIL_ACTION.ADD_BREADCRUMB_TITLE :
      return {
        ...state,
        breadcrumbTitle : action.payload.title
      }
    case DETAIL_ACTION.ADD_CONTENT_DETAIL :
      return {
        ...state,
        body : action.payload.body,
        status : action.payload.status,
        wrapper : action.payload.wrapper
      }
    case DETAIL_ACTION.ADD_CONTENT_DETAIL_FROM_QUERY_STRING :
      return {
        ...state,
        breadcrumbTitle : action.payload.breadcrumbTitle,
        body : action.payload.body,
        status : action.payload.status,
        wrapper : action.payload.wrapper
      }
    case DETAIL_ACTION.UPDATE_DETAIL_WRAPPER :
      return {
        ...state,
        breadcrumbTitle : '...',
        body : new Object,
        status : 0,
        wrapper : action.payload.wrapper
      }
    case DETAIL_ACTION.UNMOUNT_CONTENT_DETAIL : 
      return {
        breadcrumbTitle : '...',
        body : new Object,
        status : 0,
        wrapper : ''
      }
    default:
      return state
  }
}