import * as FETCH_DATA from '../actions/fetchDataAction';

const initialState = {
  load : 'pending',
  datas : new Array,
  data : new Object,
  currentPage : 0,
  status : null,
  count : 0,
  type : '',
  hasMore : true,
  totalPage : 1
};

export const planets = (state=initialState, action) => {
  switch(action.type) {
    case FETCH_DATA.LOAD_DATA_PLANETS_SUCCESS:
      return {
        ...state,
        load : 'success',
        datas : action.payload.response.data.results,
        count : action.payload.response.data.count,
        status : action.payload.response.status,
        currentPage : action.payload.response.data.next === null ? 1 : action.payload.response.data.next.split('=')[action.payload.response.data.next.split('=').length-1] - 1,
        type : action.payload.options.type
      }
    case FETCH_DATA.PUSH_DATA_PLANETS_SUCCESS:
      return {
        ...state,
        load : 'success',
        count : action.payload.response.data.count,
        currentPage : state.currentPage + 1,
        datas : state.datas.concat(action.payload.response.data.results),
        status : action.payload.response.status,
        type : action.payload.options.pushData && 'inifinite_load',
        totalPage : Math.round( action.payload.response.data.count / 10 )
      }
    case FETCH_DATA.PUSH_DATA_PLANETS_FINISHED:
      return {
        ...state,
        load : 'infinite_load_finish',
        hasMore : false
      }
    case FETCH_DATA.RESET_CURRENT_PLANETS_DATA:
      return {
        load : 'pending',
        datas : new Array,
        data : new Object,
        currentPage : 0,
        status : null,
        count : 0,
        type : '',
        hasMore : true,
        totalPage : 1
      }
    default:
      return state
  }
}