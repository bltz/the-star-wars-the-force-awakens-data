import { combineReducers } from "redux";
import { configReducer as config } from './configReducer';
import { people } from './peopleReducer';
import { films } from './filmsReducer';
import { planets } from './planetsReducer';
import { starships } from './starshipsReducer';
import { vehicles } from './vehiclesReducer';
import { species } from './speciesReducer';
import { detailReducer as detail } from './detailReducer';
import { userReducer as user } from './userReducer'

const reducers = combineReducers({
  config,
  detail,
  people,
  films,
  planets,
  starships,
  vehicles,
  species,
  user
});

export default reducers