import * as CONFIG_ACTION from '../actions/configAction';
import { mainColors } from '../../helper';

const initialState = {
  mode : 'dark',
  theme : {
    backgroundColor : mainColors.bgDark,
    color : mainColors.clLight,
    primary : '',
    navBgColor : mainColors.navBgDark,
    headIndex : mainColors.headIndexDark,
    borderColor : mainColors.borderColorDark,
    accentBg : mainColors.accentBgDark
  },
  number : 0,
  listSpiner : {
    show : false,
    index : null,
    wrapper : ''
  }
};

export const configReducer = (state=initialState, action) => {
  switch(action.type) {
    case CONFIG_ACTION.LIST_SPINER_ON:
      console.log(action.payload,'action.payload')
      return {
        ...state,
        listSpiner : {
          show : action.payload.options.show,
          index : action.payload.options.index,
          wrapper : action.payload.options.wrapper
        }
      }
    case CONFIG_ACTION.LIST_SPINER_OFF:
      return {
        ...state,
        listSpiner : {
          show : false,
          index : null,
          wrapper : ''
        }
      }
    case CONFIG_ACTION.CHANGE_THEME :
      return {
        ...state,
        mode : action.payload.mode,
        theme : {
          ...state.theme,
          accentBg : action.payload.mode === 'light' ? mainColors.accentBgLight : mainColors.accentBgDark,
          backgroundColor : action.payload.mode === 'light' ? mainColors.bgLight : mainColors.bgDark,
          color : action.payload.mode === 'light' ? mainColors.clDark : mainColors.clLight ,
          navBgColor : action.payload.mode === 'light' ? mainColors.navBgLight : mainColors.navBgDark,
          headIndex : action.payload.mode === 'light' ? mainColors.headIndexLight : mainColors.headIndexDark,
          borderColor : action.payload.mode === 'light' ? mainColors.borderColorLight : mainColors.borderColorDark
        }
      }
    default:
      return state
  }
}