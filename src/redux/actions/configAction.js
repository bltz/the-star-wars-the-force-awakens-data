export const CHANGE_THEME    = 'CHANGE_THEME'
export const LIST_SPINER_ON  = 'LIST_SPINER_ON';
export const LIST_SPINER_OFF = 'LIST_SPINER_OFF';

export const spinnerON = (options, callback) => async ( dispatch, getState ) => {
  await dispatch({ 
    type : LIST_SPINER_ON, 
    payload : {
      options
    }
  })
  return callback && callback()
}

export const spinnerOFF = () => dispatch => {
  return dispatch({ type : LIST_SPINER_OFF })
}

export const changeTheme = (mode) => async dispatch => {
  return dispatch({
    type : CHANGE_THEME,
    payload : { mode }
  })
}