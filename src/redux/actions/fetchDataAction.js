import { fetchData } from '../../api/endpoints';

export const LOAD_DATA_PEOPLE_SUCCESS     = 'LOAD_DATA_PEOPLE_SUCCESS';
export const LOAD_DATA_FILMS_SUCCESS      = 'LOAD_DATA_FILMS_SUCCESS';
export const LOAD_DATA_PLANETS_SUCCESS    = 'LOAD_DATA_PLANETS_SUCCESS';
export const LOAD_DATA_STARSHIPS_SUCCESS  = 'LOAD_DATA_STARSHIPS_SUCCESS';
export const LOAD_DATA_VEHICLES_SUCCESS   = 'LOAD_DATA_VEHICLES_SUCCESS';
export const LOAD_DATA_SPECIES_SUCCESS    = 'LOAD_DATA_SPECIES_SUCCESS';

export const PUSH_DATA_PEOPLE_SUCCESS     = 'PUSH_DATA_PEOPLE_SUCCESS';
export const PUSH_DATA_PEOPLE_FINISHED    = 'PUSH_DATA_PEOPLE_FINISHED';
export const PUSH_DATA_FILMS_SUCCESS      = 'PUSH_DATA_FILMS_SUCCESS';
export const PUSH_DATA_FILMS_FINISHED     = 'PUSH_DATA_FILMS_FINISHED';
export const PUSH_DATA_PLANETS_SUCCESS    = 'PUSH_DATA_PLANETS_SUCCESS';
export const PUSH_DATA_PLANETS_FINISHED   = 'PUSH_DATA_PLANETS_FINISHED';
export const PUSH_DATA_STARSHIPS_SUCCESS  = 'PUSH_DATA_STARSHIPS_SUCCESS';
export const PUSH_DATA_STARSHIPS_FINISHED = 'PUSH_DATA_STARSHIPS_FINISHED';
export const PUSH_DATA_VEHICEL_SUCCESS    = 'PUSH_DATA_VEHICEL_SUCCESS';
export const PUSH_DATA_VEHICEL_FINISHED   = 'PUSH_DATA_VEHICEL_FINISHED';
export const PUSH_DATA_SPECIES_SUCCESS    = 'PUSH_DATA_SPECIES_SUCCESS';
export const PUSH_DATA_SPECIES_FINISHED   = 'PUSH_DATA_SPECIES_FINISHED';

export const RESET_CURRENT_PEOPLE_DATA    = 'RESET_CURRENT_PEOPLE_DATA'
export const RESET_CURRENT_FILMS_DATA     = 'RESET_CURRENT_FILMS_DATA'
export const RESET_CURRENT_PLANETS_DATA   = 'RESET_CURRENT_PLANETS_DATA'
export const RESET_CURRENT_STARSHIPS_DATA = 'RESET_CURRENT_STARSHIPS_DATA'
export const RESET_CURRENT_VEHICLES_DATA  = 'RESET_CURRENT_VEHICLES_DATA'
export const RESET_CURRENT_SPECIES_DATA   = 'RESET_CURRENT_SPECIES_DATA'


export const getDataContainerPage = ( request, successCB ) => ( dispatch ) => {
  let temp = 0
  request.forEach((req) => {
    temp++
    return fetchData({
      type : req.type,
      key  : req.key,
      page : req.page
    }).then((res) => setTimeout(() => {
      return (
        dispatch({
          type    : req.pushData ? pushKeyCondition(req.key) : mountKeyCondition(req.key),
          payload : { response : res, options : req }
        }),
        temp === request.length && successCB && successCB()
      )
      }, req.key === 'starships' || req.key === 'vehicles' || req.key === 'species' ? 500 : 150)
    ).catch((err) => {
      // console.error(err,'errloaddata')
      return err.toString() === 'Error: Request failed with status code 404' 
             ? dispatch(infiniteLoadFinish(req.key)) : null
    })
  })
}

export const infiniteLoadFinish = (key) => dispatch => dispatch({ type : infiniteKeyFinishCondition(key) })

export const resetCurrentContainerData = (request, successCB) => async ( dispatch ) => {
  let temp = 0
  return request.forEach((req) => {
    temp++
    return (
      dispatch({ type : unMountKeyCondition(req.key) }),
      temp === request.length && successCB && successCB()
    )
  })
}

const pushKeyCondition = (key) => {
  switch(key){
    case 'people':
      return PUSH_DATA_PEOPLE_SUCCESS
    case 'films':
      return PUSH_DATA_FILMS_SUCCESS
    case 'planets':
      return PUSH_DATA_PLANETS_SUCCESS
    case 'starships':
      return PUSH_DATA_STARSHIPS_SUCCESS
    case 'vehicles':
      return PUSH_DATA_VEHICEL_SUCCESS
    case 'species':
      return PUSH_DATA_SPECIES_SUCCESS
  }
}

const infiniteKeyFinishCondition = (key) => {
  switch(key){
    case 'people':
      return PUSH_DATA_PEOPLE_FINISHED
    case 'films':
      return PUSH_DATA_FILMS_FINISHED
    case 'planets':
      return PUSH_DATA_PLANETS_FINISHED
    case 'starships':
      return PUSH_DATA_STARSHIPS_FINISHED
    case 'vehicles':
      return PUSH_DATA_VEHICEL_FINISHED
    case 'species':
      return PUSH_DATA_SPECIES_FINISHED
  }
}

const mountKeyCondition = (key) => {
  switch(key){
    case 'people':
      return LOAD_DATA_PEOPLE_SUCCESS
    case 'films':
      return LOAD_DATA_FILMS_SUCCESS
    case 'planets':
      return LOAD_DATA_PLANETS_SUCCESS
    case 'starships':
      return LOAD_DATA_STARSHIPS_SUCCESS
    case 'vehicles':
      return LOAD_DATA_VEHICLES_SUCCESS
    case 'species':
      return LOAD_DATA_SPECIES_SUCCESS
  }
}

const unMountKeyCondition = (key) => {
  switch(key){
    case 'people':
      return RESET_CURRENT_PEOPLE_DATA
    case 'films':
      return RESET_CURRENT_FILMS_DATA
    case 'planets':
      return RESET_CURRENT_PLANETS_DATA
    case 'starships':
      return RESET_CURRENT_STARSHIPS_DATA
    case 'vehicles':
      return RESET_CURRENT_VEHICLES_DATA
    case 'species':
      return RESET_CURRENT_SPECIES_DATA
  }
}