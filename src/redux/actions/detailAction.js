import { fetchData } from '../../api/endpoints';
import * as ACTION_CONFIG from './configAction';
import axios from 'axios'

export const ADD_BREADCRUMB_TITLE   = 'ADD_BREADCRUMB_TITLE';
export const ADD_CONTENT_DETAIL     = 'ADD_CONTENT_DETAIL';
export const ADD_CONTENT_DETAIL_FROM_QUERY_STRING = 'ADD_CONTENT_DETAIL_FROM_QUERY_STRING'
export const UNMOUNT_CONTENT_DETAIL = 'UNMOUNT_CONTENT_DETAIL';
export const UPDATE_DETAIL_WRAPPER = 'UPDATE_DETAIL_WRAPPER'

export const getDetailIndex = ( options, callback ) => async ( dispatch, getState ) => {
  await dispatch({ type : ADD_BREADCRUMB_TITLE, payload : { title : options.title }})
  await fetchData({
    type : 'singular',
    url : options.url
  }).then((res) => {
    options.dispatchType = ADD_CONTENT_DETAIL
    return dispatch(addDetailWrapper(res,options)).then((state) => {
      return state.detail.status === 200 && dispatch(ACTION_CONFIG.spinnerOFF()) && callback(state.detail.status)
    }).catch(() => {})
  }).catch(() => console.error(`ERROR : ${options.url} ${options.title} failed !!!`))
}

export const getDetailIndexForceRefresh = ( query, params, title ) => async ( dispatch, getState ) => {
  return axios.get(`${process.env.HTTP_URL}${params}/${query.id}/`).then((res) => {
    return dispatch(addDetailWrapper(res,{ breadcrumbTitle : title, wrapper : `${params}Wrapper`, dispatchType : ADD_CONTENT_DETAIL_FROM_QUERY_STRING }))
  })
}

const addDetailWrapper = (res, options) => ( dispatch, getState ) => {
  return new Promise((resolve,reject) => {
    dispatch({
      type : options.dispatchType,
      payload : {
        body : res.data,
        status : res.status,
        wrapper : options.wrapper,
        breadcrumbTitle : options.breadcrumbTitle ? options.breadcrumbTitle : null
      }
    })
    return resolve(getState())
  })
}

export const unmountDetailPage = () => async ( dispatch ) => {
  await dispatch({ type : UNMOUNT_CONTENT_DETAIL })
  return dispatch( ACTION_CONFIG.spinnerOFF() )
}

export const updatedetailWraper = ( wrapper, options ) => async ( dispatch, getState ) => {
  await dispatch({
    type : UPDATE_DETAIL_WRAPPER,
    payload : { wrapper : `${wrapper}Wrapper` }
  })
  return setTimeout(() => {
    return fetchData({
      type : 'singular',
      url : options.url
    }).then((res) => {
      options.dispatchType = ADD_CONTENT_DETAIL
      options.wrapper = `${wrapper}Wrapper`
      return dispatch(addDetailWrapper(res,options))
    })
  },250) 
}