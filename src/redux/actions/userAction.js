import uuidv4 from 'uuid/v4'
export const IMPORT_VERIFY_DATA = 'IMPORT_VERIFY_DATA'
export const EXPORT_VERIFY_DATA = 'EXPORT_VERIFY_DATA'

const importVerifyData = ( options ) => dispatch => {
  return new Promise((resolve) => {
    dispatch({
      type : IMPORT_VERIFY_DATA,
      payload : {
        name : options.name,
        uuid : options.uuid,
      }
    })
    resolve()
  })
}

export const checkVerifyData = (successCB, failedCB) => async ( dispatch, getState ) => {
  await localStorage.getItem('the_force_Awakens_Data') !== null 
        ? dispatch(importVerifyData(JSON.parse(localStorage.getItem('the_force_Awakens_Data')))).then(() => {
          return successCB && successCB(getState().user)
        })
        : failedCB && failedCB()
}

export const generateUserVerify = ( yourname, callback ) => async ( dispatch, getState ) => {
  await localStorage.setItem('the_force_Awakens_Data', JSON.stringify({ name : yourname, uuid : uuidv4() }))
  return dispatch(checkVerifyData( callback, null ))
}

export const exportVerifyData = ( callback ) => async ( dispatch ) => {
  await localStorage.removeItem('the_force_Awakens_Data')
  await dispatch({ type : EXPORT_VERIFY_DATA })
  callback && callback()
}